from django.test import TestCase, Client
from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from django.urls import reverse

class TestModels(TestCase):
	def setUp(self):
		self.user_data = {
			"username": "testuser",
			"password": "testpass123",
		}

	def test_user_create(self):
		user = User.objects.create_user(**self.user_data)

		self.assertEqual(user.username, self.user_data["username"])
		self.assertTrue(user.check_password(self.user_data["password"]))		

	def test_user_create_invalid_username(self):
		user = User.objects.create_user(**self.user_data)

		no_username_user = self.user_data.copy()
		no_username_user["username"] = ""

		with self.assertRaises(IntegrityError):
			User.objects.create_user(**self.user_data)
		with self.assertRaises(ValueError):
			User.objects.create_user(**no_username_user)

class TestViews(TestCase):
	def setUp(self):
		self.client = Client()

		self.user_data = {
			"username": "testuser",
			"password1": "testpass123",
			"password2": "testpass123",
		}

		self.user_login = {
			"username": "testuser",
			"password": "testpass123",
		}


	def test_register(self):
		response = self.client.post(reverse("register"), self.user_data)
		user = User.objects.get(username=self.user_data["username"])

		self.assertEqual(user.username, self.user_data["username"])
		self.assertTrue(user.check_password(self.user_data["password1"]))
		self.assertEqual(response.status_code, 302)

	def test_login(self):
		user = User.objects.create_user(**self.user_login)

		response = self.client.post(reverse("login"), self.user_login)

		self.assertTrue(user.is_authenticated)		
		self.assertEqual(response.status_code, 302)


	def test_logout(self):
		user = User.objects.create_user(**self.user_login)

		response = self.client.post(reverse("login"), self.user_login)

		self.assertTrue(user.is_authenticated)		
		self.assertEqual(response.status_code, 302)

		response = self.client.post(reverse("logout"))

		user = response.wsgi_request.user

		self.assertFalse(user.is_authenticated)
		self.assertEqual(response.status_code, 302)


