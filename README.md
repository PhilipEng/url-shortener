
# URL Shortener

A simple url shortener created using Django. To create shortened urls, you must first create a 
user account. After a short url is created, it can be viewed/deleted from 'My Links'.


# How to run Url Shortener:

### Pull Docker image via Gitlab:
1. Ensure Docker is installed on your system.
2. Pull image:    

    docker pull registry.gitlab.com/philipeng/url-shortener/urlshortener:0.1
3. Run docker image:  

    docker run --name urlshortener -d -p 8000:8000 registry.gitlab.com/philipeng/url-shortener/urlshortener:0.1
4. Access the exposed port at http://127.0.0.1:8000

### To build and run docker container from source files:
1. Ensure Docker is installed on your system.
2. Navigate to project root.
3. Build and start image:
    docker compose up --build
4. Access the exposed port at http://127.0.0.1:8000

### To run as Django app from source:
1. Ensure python is installed, along with the packages in requirements.txt
2. Navigate to project root.
3. Start Django server:
    python manage.py runserver
4. Access the exposed port at http://127.0.0.1:8000

To run the accompanying unit tests:    
    python manage.py test




