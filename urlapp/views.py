from django.shortcuts import render, redirect
from urlapp.models import Link
from urlapp.forms import LinkForm
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
import uuid

class LinkListView(ListView):
    model = Link
    template_name = 'urlapp/list.html'
    context_object_name = 'link_list'

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return redirect('register')
        return Link.objects.filter(owner=self.request.user).order_by('-visits')

    def get(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect('register')
        return super().get(request, *args, **kwargs)

def home(response):
    return render(response, 'urlapp/home.html', {})

def index(response):
    return render(response, 'urlapp/base.html', {})

def make_short_url(request):
    if not request.user.is_authenticated:
        return redirect('register')

    form = LinkForm(request.POST)
    link_id = ""

    if request.method == "POST":
        if form.is_valid():
            NewLink = form.save(commit=False)
            link_id = str(uuid.uuid4())[:6]
            if Link.objects.filter(link_id = link_id).first() is not None:
                link_id = str(uuid.uuid4())[:6]
            NewLink.link_id = link_id
            NewLink.short_link = request.get_host() + link_id
            NewLink.owner = request.user
            NewLink.save()
        else:
            form = LinkForm()
            link_id = "Invalid URL"

    return render(request, "urlapp/shorten.html", {'form': form, 'link_id': link_id})

def visit_short_url(request, link_id):
    link = get_object_or_404(Link, link_id=link_id)
    link.add_visit()
    return redirect(link.long_link)

def delete_link(request, link_id):
    
    link = Link.objects.get(link_id=link_id)
    if(request.user == link.owner):
        link.delete()
    
    return redirect('link_list')


