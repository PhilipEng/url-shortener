from django.test import TestCase, Client
from urlapp.models import Link
from urlapp.views import make_short_url
from urlapp.forms import LinkForm
from django.contrib.auth.models import User
from django.urls import reverse

class TestViews(TestCase):
	def setUp(self):
		self.client = Client()
		self.user_data = {
			"username": "testuser",
			"password": "testpass123",
		}

		self.link_data = {
			"long_link": "http://www.google.com/"
		}

	def test_home_view(self):
		response = self.client.get(reverse("home"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "urlapp/home.html")

	def test_register_view(self):
		response = self.client.get(reverse("register"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "accounts/register.html")

	def test_login_view(self):
		response = self.client.get(reverse("login"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "registration/login.html")

	def test_shorten_view_no_auth(self):
		#When user is not authenticated, should be redirected to register

		response = self.client.get(reverse("shorten"))
		self.assertEqual(response.status_code, 302)

	def test_shorten_view_auth(self):
		#When user is logged in, should be able to access shorten
		user = User.objects.create_user(**self.user_data)

		response = self.client.post(reverse("login"), self.user_data)

		self.assertTrue(user.is_authenticated)

		response = self.client.get(reverse("shorten"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "urlapp/shorten.html")

	def test_list_view_no_auth(self):
		#When user is not authenticated, should be redirected to register

		response = self.client.get(reverse("link_list"))
		self.assertEqual(response.status_code, 302)

	def test_shorten_list_auth(self):
		#When user is logged in, should be able to access shorten
		user = User.objects.create_user(**self.user_data)

		response = self.client.post(reverse("login"), self.user_data)

		self.assertTrue(user.is_authenticated)

		response = self.client.get(reverse("link_list"))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, "urlapp/list.html")

	def test_create_link(self):
		user = User.objects.create_user(**self.user_data)

		response = self.client.post(reverse("login"), self.user_data)

		self.assertTrue(user.is_authenticated)
		form = LinkForm
		form.long_link = self.user_data
		response = self.client.post(reverse("shorten"), self.link_data)

		self.assertEqual(response.status_code, 200)
		link = Link.objects.get(long_link=self.link_data["long_link"])

		self.assertEqual(link.long_link, self.link_data["long_link"])
		self.assertEqual(link.owner, user)
		self.assertEqual(link.visits, 0)

	def test_visit_short_link(self):
		#Creating Link
		user = User.objects.create_user(**self.user_data)

		response = self.client.post(reverse("login"), self.user_data)

		self.assertTrue(user.is_authenticated)
		form = LinkForm
		form.long_link = self.user_data
		response = self.client.post(reverse("shorten"), self.link_data)

		self.assertEqual(response.status_code, 200)
		link = Link.objects.get(long_link=self.link_data["long_link"])

		#Checking Link created successfully
		self.assertEqual(link.long_link, self.link_data["long_link"])
		self.assertEqual(link.owner, user)
		self.assertEqual(link.visits, 0)

		link_id = link.link_id
		short_link = link.short_link

		#Visiting short_url
		response = self.client.get("/"+link_id)

		#Verifying destination
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response["Location"], self.link_data["long_link"])

		#Checking that visits has incremented
		link = Link.objects.get(long_link=self.link_data["long_link"])
		self.assertEqual(link.visits, 1)

	def test_del_link(self):
		#Creating Link
		user = User.objects.create_user(**self.user_data)

		response = self.client.post(reverse("login"), self.user_data)

		self.assertTrue(user.is_authenticated)
		form = LinkForm
		form.long_link = self.user_data
		response = self.client.post(reverse("shorten"), self.link_data)

		self.assertEqual(response.status_code, 200)
		link = Link.objects.get(long_link=self.link_data["long_link"])

		#Checking Link created successfully
		self.assertEqual(link.long_link, self.link_data["long_link"])
		self.assertEqual(link.owner, user)
		self.assertEqual(link.visits, 0)

		link_id = link.link_id

		# Deleting Link
		response = self.client.get("/list/delete/"+link_id)

		link = Link.objects.filter(link_id=link_id).first()

		#Check that Link is deleted
		self.assertIsNone(link)





