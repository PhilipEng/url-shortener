from django import forms
from .models import Link

class LinkForm(forms.ModelForm):
    long_link = forms.CharField(label='Enter Long URL')

    class Meta:
        model = Link
        fields = ['long_link']