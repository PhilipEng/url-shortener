from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('shorten', views.make_short_url, name='shorten'),
    path('list/', views.LinkListView.as_view(), name='link_list'),
    path('<str:link_id>', views.visit_short_url, name="visit_short_url"),
    path('list/delete/<link_id>', views.delete_link, name='delete_link')
]