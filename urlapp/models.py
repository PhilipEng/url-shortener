from django.db import models
from django.contrib.auth.models import User

class Link(models.Model):
	long_link = models.URLField(max_length=1000)
	short_link = models.CharField(max_length=100)
	link_id = models.CharField(unique=True, max_length=6)
	visits = models.IntegerField(("Number of visits"), default=0)
	owner = models.ForeignKey(User, on_delete=models.CASCADE)

	def add_visit(self, number=1):
		self.visits += number
		self.save(update_fields=["visits"])

	def __str__(self):
		return self.link_id